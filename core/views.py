# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.shortcuts import render
from django.http import HttpResponse
from django.views.decorators.csrf import csrf_exempt
import json
import re

import urllib
import textract

def getEmail(inputString, debug=False): 
	email = None
	try:
		pattern = re.compile(r'\w+[.|\w]\w+@\w+[.]\w+[.|\w+]\w+')
		matches = pattern.findall(inputString) # Gets all email addresses as a list
		email = matches
	except Exception as e:
		print(e)
	return email

@csrf_exempt
def parse(request):
	if request.method == "POST":
		urlsArray = request.POST.getlist('urlsArray')

		if (len(urlsArray) == 0):
			return HttpResponse(json.dumps({}), status=404)

		finaldict = []

		for url in urlsArray:
			status = 200
			message = "OK"
			try:
				file = urllib.urlretrieve(url)
			except Exception as e:
				if (e.errno == 2):
					status = 404
					message = e.args[1]
			try:
				text = textract.process(file[0])
				email = getEmail(text)
			except textract.exceptions.ExtensionNotSupported:
				status = 415
				message = 'Extension Not Supported'
			except textract.exceptions.MissingFileError:
				status = 404
				message = 'Missing File Error'
			except Exception as e:
				status = 00
				message = e
			
			jsondict = {}
			
			jsondict['url'] = url
			if status == 200:
				if len(email) != 0:
					jsondict['email'] = email[0]
				else:
					jsondict['email'] = None
			jsondict['status'] = status
			jsondict['message'] = message
			finaldict.append(jsondict)

		urllib.urlcleanup()
		print(file[0])

		# HttpResponse.__setitem__('Access-Control-Allow-Origin', '*')

		return HttpResponse(json.dumps(finaldict))